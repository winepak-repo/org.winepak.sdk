build-runtime: true

id: org.winepak.BaseSdk
id-platform: org.winepak.BasePlatform
branch: 4.0

runtime: org.freedesktop.Platform
runtime-version: 19.08
sdk: org.freedesktop.Sdk

sdk-extensions:
  - org.freedesktop.Sdk.Debug
  - org.freedesktop.Sdk.Locale
  - org.freedesktop.Sdk.Docs

add-extensions:
  org.winepak.BaseSdk.Docs:
    directory: share/runtime/docs
    bundle: true
    autodelete: true
    no-autodownload: true

  org.winepak.BaseSdk.Extension:
    directory: lib/sdk
    version: 4.0
    subdirectories: true
    autodelete: true
    no-autodownload: true

  org.winepak.BasePlatform.Compat.i386:
    directory: lib/i386-linux-gnu
    version: 4.0
    versions: 4.0
    add-ld-path: lib
    subdirectories: false
    autodelete: false
    no-autodownload: false

  org.winepak.BasePlatform.Gecko:
    directory: lib/gecko
    version: 4.0
    versions: 4.0
    subdirectories: false
    autodelete: false

  org.winepak.BasePlatform.Mono:
    directory: lib/mono
    version: 4.0
    versions: 4.0
    subdirectories: false
    autodelete: false

  org.winepak.BasePlatform.Extension:
    directory: lib/extension
    version: 4.0
    subdirectories: true
    autodelete: true
    no-autodownload: true

platform-extensions:
  - org.freedesktop.Platform.Locale

inherit-extensions:
  - org.freedesktop.Platform.GL
  - org.freedesktop.Platform.Timezones
  - org.freedesktop.Platform.GStreamer
  - org.freedesktop.Platform.Icontheme
  - org.freedesktop.Platform.VAAPI.Intel
  - org.gtk.Gtk3theme


inherit-sdk-extensions:
  - org.freedesktop.Sdk.Extension

finish-args:
  - --sdk=org.winepak.BaseSdk//4.0
  - --runtime=org.winepak.BasePlatform//4.0
  - --allow=multiarch
  - --env=PATH=/app/bin:/usr/bin:/usr/lib/i386-linux-gnu/bin
  - --env=XDG_DATA_DIRS=/app/share:/usr/share:/usr/share/runtime/share:/run/host/share
  - --env=WINEDEBUG=-all
  - --env=WINEPREFIX=/var/data/wine
  # dconf and kdeglobals are not necessary but some linux apps require them, so it may be useful for some weird usecases where apps are aware that they run on linux
  - --env=DCONF_USER_CONFIG_DIR=.config/dconf
  - --filesystem=xdg-config/kdeglobals:ro
  - --filesystem=xdg-run/dconf
  - --filesystem=~/.config/dconf:ro

cleanup:
  - /docs
  - /man
  - /share/man

cleanup-platform:
  - /docs
  - /include
  - /share/aclocal
  - /share/pkgconfig
  - /lib/pkgconfig

build-options:
  cflags: -O2 -pipe -fstack-protector-strong -fno-plt -frecord-gcc-switches -D_FORTIFY_SOURCE=2 -msse -msse2 -msse3 -mssse3 -mfpmath=sse -msahf -mprfchw -mcx16
  cxxflags: -O2 -pipe -fstack-protector-strong -fno-plt -frecord-gcc-switches -D_FORTIFY_SOURCE=2 -msse -msse2 -msse3 -mssse3 -mfpmath=sse -msahf -mprfchw -mcx16
  ldflags: -fstack-protector-strong -Wl,-z,relro,-z,now
  env:
    V: '1'

modules:
  - "mingw.yml"

  - name: platform-setup
    buildsystem: simple
    build-commands:
      - mkdir -p /usr/share/runtime/docs
      - mkdir -p /usr/lib/sdk
      - mkdir -p /usr/lib/gecko
      - mkdir -p /usr/lib/wine
      - mkdir -p /usr/lib/extension

  - name: lmdb
    subdir: libraries/liblmdb
    buildsystem: simple
    build-commands:
      - make
      - make prefix=/usr install
    sources:
      - type: archive
        url: https://github.com/LMDB/lmdb/archive/LMDB_0.9.24.tar.gz
        sha256: 44602436c52c29d4f301f55f6fd8115f945469b868348e3cddaf91ab2473ea26

  - name: openldap
    config-opts:
      - --disable-static
      - --disable-bdb
      - --disable-hdb
      - --enable-mdb
    sources:
      # OpenLDAP required for Wine
      # Due to the licencing of BDB v6.* we must stick with OpenLDAP 2.4.46
      # FTP isn't support in flatpak & flatpak-builder so we use he https mirror
      # https://www.openldap.org/software/download/OpenLDAP/openldap-release/
      - type: archive
        url: https://www.openldap.org/software/download/OpenLDAP/openldap-release/openldap-2.4.48.tgz
        sha256: d9523ffcab5cd14b709fcf3cb4d04e8bc76bb8970113255f372bc74954c6074d

  - name: krb5
    subdir: src
    config-opts:
      - --prefix=/usr
      - --enable-shared
      - --enable-dns-for-realm
      - --with-ldap
      - --with-lmdb
    sources:
      - type: archive
        url: https://web.mit.edu/kerberos/dist/krb5/1.17/krb5-1.17.tar.gz
        sha256: 5a6e2284a53de5702d3dc2be3b9339c963f9b5397d3fbbc53beb249380a781f5

  - name: glu
    sources:
      - type: archive
        url: https://gitlab.freedesktop.org/mesa/glu/-/archive/glu-9.0.1/glu-glu-9.0.1.tar.gz
        sha256: d02703066406cdcb54b99119b71f869cb1af2bbd403b928f3191daccca874377

  - name: gphoto2
    sources:
      - type: archive
        url: https://downloads.sourceforge.net/project/gphoto/libgphoto/2.5.23/libgphoto2-2.5.23.tar.bz2
        sha256: d8af23364aa40fd8607f7e073df74e7ace05582f4ba13f1724d12d3c97e8852d

  - name: v4l-utils
    config-opts:
      - --disable-static
      - --disable-doxygen-doc
      - --disable-v4l-utils
    sources:
      - type: archive
        url: https://linuxtv.org/downloads/v4l-utils/v4l-utils-1.16.7.tar.bz2
        sha256: ee917a7e1af72c60c0532d9fdb9e48baf641d427aa7b009a9b2ca821f9e8e0d9

  - name: pcap
    sources:
      - type: archive
        url: https://www.tcpdump.org/release/libpcap-1.9.0.tar.gz
        sha256: 2edb88808e5913fdaa8e9c1fcaf272e19b2485338742b5074b9fe44d68f37019

  - name: oss                                  # flatpak doesnt support oss
    buildsystem: simple
    disabled: true
    build-commands:
      - mkdir build
      - cd build && ../configure --regparm
      - cd build && make build
      - cd build && make copy
    sources:
      - type: archive
        url: http://www.4front-tech.com/developer/sources/stable/gpl/oss-v4.2-build2019-src-gpl.tar.bz2
        sha256: e41afc0a442c9ebb5d1c683fbda24c58c1e98838ecf137bb849c6f20ba131683
      - type: patch
        path: oss/oss4_sys-libs_glibc-2.23_ossdetect_fix_git.patch

  - name: sane
    config-opts:
    # from aur
    - --enable-libusb_1_0
    - --enable-pthread
    - --enable-avahi
    - --disable-locking
    - --disable-rpath
    sources:
      - type: archive
        url: https://gitlab.com/sane-project/backends/uploads/9e718daff347826f4cfe21126c8d5091/sane-backends-1.0.28.tar.gz
        sha256: 31260f3f72d82ac1661c62c5a4468410b89fb2b4a811dabbfcc0350c1346de03

  - name: capi
    sources:
      - type: archive
        url: "http://deb.debian.org/debian/pool/main/libc/libcapi20-3/libcapi20-3_3.27.orig.tar.bz2"
        sha256: d8e423d5adba1750f511a2c088296db2a8a2e1e9209401871b01ce411d8ac583

  - name: gsm
    buildsystem: simple
    build-commands:
      - patch -p0 -i gsm-shared.patch
      - install -m755 -d /usr/include/gsm
      - make CCFLAGS="-c ${CFLAGS} -fPIC"
      - make -j1 INSTALL_ROOT=/usr GSM_INSTALL_INC=/usr/include/gsm install
    sources:
      - type: archive
        url: http://www.quut.com/gsm/gsm-1.0.18.tar.gz
        sha256: 04f68087c3348bf156b78d59f4d8aff545da7f6e14f33be8f47d33f4efae2a10
      - type: file
        path: gsm/gsm-shared.patch
        sha256: 412e26ab0a0fe187f7a8a16400921ef745a8af5440006136e4d3169d30875f50

  - name: gssapi
    disabled: true                             # seems to not be needed
    make-install-args:
      - DESTDIR=/
    config-opts:
      # from aur
      - --prefix=/usr
      - --sbindir=/usr/bin
      - --libexecdir=/usr/lib/ssh
      - --sysconfdir=/etc/ssh
      - --with-ldns
      - --without-libedit
      - --with-ssl-engine
      - --without-pam
      - --with-privsep-user=nobody
      - --with-kerberos5=/usr
      - --with-xauth=/usr/bin/xauth
      - --with-mantype=man
      - --with-md5-passwords
      - --with-pid-dir=/run
      - --with-gssapi
    sources:
      - type: archive
        url: https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-7.5p1.tar.gz
        sha256: 9846e3c5fab9f0547400b4d2c017992f914222b3fd1f8eee6c7dc6bc5e59f9f0
      - type: patch
        path: gssapi/openssl-1.1.0.patch
      - type: patch
        path: gssapi/get_canonical_hostname.patch
      - type: patch
        path: gssapi/gssapi.patch
      - type: patch
        path: gssapi/gssapi-openssl-1.1.0.patch
    modules:
      - name: ncurses
        buildsystem: autotools
        config-opts:
          - '--with-shared'
        sources:
          - type: archive
            url: https://ftp.gnu.org/gnu/ncurses/ncurses-6.1.tar.gz
            sha256: aa057eeeb4a14d470101eff4597d5833dcef5965331be3528c08d99cebaa0d17

  - name: FAudio #https://www.winehq.org/announce/4.3
    buildsystem: cmake-ninja
    config-opts:
      - -DCMAKE_BUILD_TYPE=Release
      - -DFFMPEG=ON
    sources:
      - type: archive
        url: "https://github.com/FNA-XNA/FAudio/archive/19.09.tar.gz"
        sha256: 754b5ccc239bc1be4046d254748cface915e653aec179424fd452255a7082677

  - name: opencl-headers
    buildsystem: simple
    build-commands:
      - cp -R CL /usr/include/CL
    sources:
      - type: git
        url: https://github.com/KhronosGroup/OpenCL-Headers.git
        commit: 0d5f18c6e7196863bc1557a693f1509adfcee056

  - name: cabextract
    buildsystem: autotools
    sources:
      - type: archive
        url: https://www.cabextract.org.uk/cabextract-1.9.1.tar.gz
        sha256: afc253673c8ef316b4d5c29cc4aa8445844bee14afffbe092ee9469405851ca7

  - name: unrar
    buildsystem: simple
    build-commands:
      - make
      - make install
    sources:
      - type: archive
        url: https://www.rarlab.com/rar/unrarsrc-5.8.1.tar.gz
        sha256: 035f1f436f0dc2aea09aec146b9cc3e47ca2442f2c62b4ad9374c7c9cc20e632

  - name: innoextract
    buildsystem: cmake
    sources:
      - type: git
        url: https://github.com/dscharrer/innoextract.git
        tag: 1.7
    modules:
      - name: boost
        buildsystem: simple
        build-commands:
          - ./bootstrap.sh --prefix=/usr --with-libraries=iostreams,filesystem,date_time,system,program_options
          - ./b2 headers
          - ./b2 install
        sources:
          - sha256: 8f32d4617390d1c2d16f26a27ab60d97807b35440d45891fa340fc2648b04406
            type: archive
            url: https://dl.bintray.com/boostorg/release/1.69.0/source/boost_1_69_0.tar.bz2

  - name: unshield
    buildsystem: cmake
    sources:
      - type: archive
        url: https://github.com/twogood/unshield/archive/1.4.3.tar.gz
        sha256: aa8c978dc0eb1158d266eaddcd1852d6d71620ddfc82807fe4bf2e19022b7bab

  - name: metainfo
    buildsystem: simple
    build-commands:
      - install -Dm444 org.winepak.Platform.appdata.xml /usr/share/appdata/org.winepak.Platform.appdata.xml
      - install -Dm444 org.winepak.Sdk.appdata.xml /usr/share/appdata/org.winepak.Sdk.appdata.xml
      - appstream-compose --basename=org.winepak.BasePlatform --prefix=/usr --origin=flatpak org.winepak.Platform
      - appstream-compose --basename=org.winepak.BaseSdk --prefix=/usr --origin=flatpak org.winepak.Sdk
    sources:
      - type: file
        path: org.winepak.Sdk.appdata.xml
      - type: file
        path: org.winepak.Platform.appdata.xml
